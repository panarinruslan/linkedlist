class ListElement:
    def __init__(self, list_el):

        self.cur_value = list_el
        self.next_val = None
        self.index = None

    @property
    def cur_value(self):
        return self._cur_value

    @cur_value.setter
    def cur_value(self, list_el):
        if not isinstance(list_el, int):
            raise TypeError(f"{list_el} must be a positive integer")
        elif list_el < 0:
            raise ValueError(f"{list_el} must be a positive integer")

        self._cur_value = list_el


class LinkedList:
    def __init__(self):
        self.first = None
        self.cur_el = None
        self.last = None
        self.counter = 0

    def add_el(self, new_el):

        if not isinstance(new_el, ListElement):
            new_el = ListElement(new_el)

        if self.first is None:
            self.first = new_el
            self.first.index = self.counter
        else:
            self.last.next_val = new_el

        self.last = new_el
        self.last.index = self.counter
        self.counter += 1

    def list_len(self):
        return self.counter

    def __next__(self):
        out_val = self.cur_el
        if self.cur_el is None:
            raise StopIteration
        self.cur_el = self.cur_el.next_val
        return out_val

    def __iter__(self):
        self.cur_el = self.first
        return self

    def __repr__(self):
        current_el = self.first
        out_str = "["
        while current_el is not None:
            if self.list_len() - 1 != current_el.index:
                out_str = f'{out_str}{str(current_el.cur_value)}, '
            else:
                out_str = f'{out_str}{str(current_el.cur_value)}]'

            current_el = current_el.next_val

        return out_str

    def func_in(self, value):
        if not isinstance(value, int):
            raise TypeError(f"{value} must be a positive integer")
        elif value < 0:
            raise ValueError(f"{value} must be a positive integer")

        current_el = self.first

        while current_el is not None:
            if current_el.cur_value == value:
                return True
            current_el = current_el.next_val

        return False

    def get_object_by_index(self, index_val):
        if not isinstance(index_val, int):
            raise TypeError(f"Index value must be a positive integer")
        if abs(index_val) >= self.counter:
            raise ValueError(f"Index bigger than list length")

        current_el = self.first

        while current_el is not None:
            if index_val < 0:
                index_val = self.counter + index_val
            if current_el.index == index_val:
                return current_el
            current_el = current_el.next_val

    def get_value(self, index_val):
        return self.get_object_by_index(index_val).cur_value

    def get_object_by_value(self, value):
        if not isinstance(value, int):
            raise TypeError(f"Index value must be a positive integer")

        current_el = self.first

        while current_el is not None:
            if current_el.cur_value == value:
                return current_el
            current_el = current_el.next_val

    def get_index(self, value):
        return self.get_object_by_value(value).index

    def upload_list_from_file(self, file):
        with open(f'{file}') as f:
            line = f.readline()
            while line or line != '':
                try:
                    int(line)
                except TypeError:
                    raise TypeError(f'{line} this is not a integer')
                self.add_el(int(line))
                line = f.readline()

    def save_to_file(self, file):
        with open(f'{file}', 'w+') as f:
            for element in self:
                f.write(str(element.cur_value) + "\n")


def sorted_function(linked_list):

    sorted_list = LinkedList()
    sorting = True

    while sorting:
        compare_element = linked_list.get_value(0)
        sorting = False
        for el_list in linked_list:
            if el_list.index != 0:
                if compare_element >= el_list.cur_value:
                    sorting = True
                    sorted_list.add_el(el_list)
                else:
                    sorted_list.add_el(compare_element)
                    compare_element = el_list.cur_value

        sorted_list.add_el(compare_element)

        linked_list = sorted_list
        if sorting:
            sorted_list = LinkedList()

    return linked_list


if __name__ == "__main__":
    container = LinkedList()
    container.upload_list_from_file('input.txt')
    sort_list = sorted_function(container)
    print('___sorted list___\n', f'{sort_list}', '\n')
    sort_list.save_to_file('output.txt')

    print('___get element by index___\n', f'{container.get_value(2)}', '\n')
    print('___get element by value return index___\n', f'{container.get_index(5345)}', '\n')
    print('___check containing element___\n', f'{container.func_in(323423535)}', '\n')


